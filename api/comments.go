package handler

import (
	"context"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"

	"github.com/jackc/pgx/v5"
)

const template_mainBody = `<html>
<body>
	{{if len .Comments | lt 0}}
		{{template "lista" .Comments}}
	{{else}}
		<p>No comment
	{{end}}
	{{if .Author}}
		{{template "post_comment" .Author}}
	{{else}}
		{{template "log_to_comment"}}
	{{end}}
</body>
</html>`

const template_lista = `
<ul>
	{{range $comment := .}}
		<li>{{$comment.Id}} - {{$comment.Comment}}</li>
	{{end}}
</ul>`

const template_post_comment = `<p>You are logged as <strong>{{.Name}}</strong>, soon we will be adding comments`
const template_log_to_comment = ``

type Comment struct {
	Id      int
	Comment string
}

type Author struct {
	Id   int
	Name string
}

func createTemplate() (*template.Template, error) {
	type templatePair struct {
		name     string
		template string
	}
	templateBase, err := template.New("mainBody").Parse(template_mainBody)
	if err != nil {
		return nil, err
	}

	for _, pair := range []templatePair{{"lista", template_lista}, {"post_comment", template_post_comment}, {"log_to_comment", template_log_to_comment}} {
		_, err = templateBase.New(pair.name).Parse(pair.template)
		if err != nil {
			return nil, err
		}
	}

	return templateBase, nil
}

func retrieveCommentFromDatabase() ([]Comment, error) {
	conn, err := pgx.Connect(context.Background(), os.Getenv("POSTGRES_URL"))
	if err != nil {
		return nil, err
	}

	rows, err := conn.Query(context.Background(), "select id, comment from hmm")

	if err != nil {
		return nil, err
	}
	var linhas []Comment

	for rows.Next() {
		var id int
		var comment_from_db string
		err := rows.Scan(&id, &comment_from_db)

		if err != nil {
			log.Printf("failed during comment fetching: <%s>\n", err.Error())
			break
		}
		linhas = append(linhas, Comment{Id: id, Comment: comment_from_db})
	}

	return linhas, nil
}

func Comments(w http.ResponseWriter, r *http.Request) {
	t, err := createTemplate()
	if err != nil {
		handleErr(w, err)
		return
	}

	linhas, err := retrieveCommentFromDatabase()
	if err != nil {
		handleErr(w, err)
		return
	}

	var author *Author = nil //&Author{Id: 123, Name: "abc"} // for debug purpose for now

	w.Header().Add("Content-type", "text/html")
	e := t.Execute(w, struct {
		Comments []Comment
		Author   *Author
	}{Comments: linhas, Author: author})
	if e != nil {
		log.Println(e)
	}
}

func handleErr(w http.ResponseWriter, err error) {
	log.Printf("failed with %s\n", err.Error())
	w.Header().Add("Content-type", "text/html")
	w.WriteHeader(http.StatusInternalServerError)
	log.Printf("an error happened %s\n", err.Error())

	fmt.Fprintln(w, `
		<html>
			<body>
				<h1>Oopsie daisie?</h1>
				<p>something bad happened at server side
			</body>
		</html>
		`)
}

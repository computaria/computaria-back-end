package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/joho/godotenv"
	handler "gitlab.com/computaria/computaria-back-end/api"
)

type myint struct{ int }

func (v *myint) stringfy() string {
	return strconv.Itoa(v.int)
}

func main() {
	err := godotenv.Load()

	if err != nil {
		log.Fatalf("Error loading .env file: %s", err.Error())
	}
	port := &myint{8081}

	http.HandleFunc("/", handler.Comments)

	fmt.Println("tentando escutar na porta " + port.stringfy())

	log.Fatal(http.ListenAndServe(":"+port.stringfy(), nil))
	fmt.Println("servindo na porta " + port.stringfy())
}
